#!/bin/bash

#!/bin/bash

set -e -u

sed -i 's/#\(en_US\.UTF-8\)/\1/' /etc/locale.gen
locale-gen

ln -sf /usr/share/zoneinfo/UTC /etc/localtime

usermod -s /usr/bin/bash root
cp -aTv /etc/skel/ /root/

cp -aTv /etc/skel/.X11/ /etc/X11/
cp -aTv /etc/skel/setup /usr/bin/setup

useradd -m -p "" -g users -G "adm,audio,floppy,log,network,rfkill,scanner,storage,optical,power,wheel" -s /bin/bash liveuser
chown -R liveuser:users /home/liveuser
chown -R root:root /etc/sudoers.d
chmod +x /root/Desktop/*.desktop

sed -i 's/#\(PermitRootLogin \).\+/\1yes/' /etc/ssh/sshd_config
sed -i "s/#Server/Server/g" /etc/pacman.d/mirrorlist
sed -i 's/#\(Storage=\)auto/\1volatile/' /etc/systemd/journald.conf

sed -i 's/#\(HandleSuspendKey=\)suspend/\1ignore/' /etc/systemd/logind.conf
sed -i 's/#\(HandleHibernateKey=\)hibernate/\1ignore/' /etc/systemd/logind.conf
sed -i 's/#\(HandleLidSwitch=\)suspend/\1ignore/' /etc/systemd/logind.conf

# mkdir -pv /usr/share/backgrounds/
# cp -aTv /etc/skel/wallpapers /usr/share/backgrounds/primearch

systemctl enable pacman-init.service choose-mirror.service
systemctl enable graphical.target
systemctl set-default graphical.target

#start system daemons
systemctl enable acpid
systemctl enable ntpd

# network managment
systemctl enable avahi-daemon
systemctl enable wpa_supplicant
systemctl enable NetworkManager

