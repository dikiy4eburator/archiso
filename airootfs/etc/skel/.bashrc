#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto -l'
PS1='[\u@\h \W]\$ '

export EDITOR="nvim"
export VISUAL="code"
