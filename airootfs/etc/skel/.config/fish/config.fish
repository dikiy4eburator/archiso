# neofetch

set -gx EDITOR nvim
set -gx VISUAL code

set -gx BROWSER firefox
